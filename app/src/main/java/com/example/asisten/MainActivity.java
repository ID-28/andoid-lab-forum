package com.example.asisten;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.example.asisten.Modul.ModelModul;

public class MainActivity extends AppCompatActivity {

    public static final String BASE_URL = "http://192.168.42.81/lab-forum/public/api/";
//    public static final String BASE_URL = "http://192.168.100.16/lab-forum/public/api/";

    private static MainActivity mInstance;
    private static Context mCtx;

    public MainActivity(Context context) {
        mCtx = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
//start from here for call to other class
    public static synchronized MainActivity getInstance(Context context){
        if (mInstance == null){
            mInstance = new MainActivity(context);
        }
        return mInstance;
    }

    public boolean logout(){
        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }
// end here
    public void Pindah(View view) {
        Intent intent = new Intent(MainActivity.this, ModelModul.class);
        startActivity(intent);
    }

    public void Pindah1(View view) {
        Intent intent = new Intent(MainActivity.this, ModelModul.class);
        startActivity(intent);
    }

    public void Pindah2(View view) {
        Intent intent = new Intent(MainActivity.this, ModelModul.class);
        startActivity(intent);
    }

    public void Pindah3(View view) {
        Intent intent = new Intent(MainActivity.this, ModelModul.class);
        startActivity(intent);
    }

    public void Pindah4(View view) {
        Intent intent = new Intent(MainActivity.this, ModelModul.class);
        startActivity(intent);
    }

    public void Pindah5(View view) {
        Intent intent = new Intent(MainActivity.this, ModelModul.class);
        startActivity(intent);
    }
}