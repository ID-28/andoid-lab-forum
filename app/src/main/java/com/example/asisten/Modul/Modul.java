package com.example.asisten.Modul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.asisten.ApiService;
import com.example.asisten.MainActivity;
import com.example.asisten.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Modul extends AppCompatActivity {
    ArrayList<ModelModul> data_modul = new ArrayList<ModelModul>();
    ListView listview;
    ListModul adapter;

    TextView ModKosong;

    ProgressDialog loading;

    String id_praktikum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modul);

        id_praktikum = getIntent().getStringExtra("id_praktikum");

        listview = (ListView) findViewById(R.id.list_modul);
        listview.setDividerHeight(0);

        ModKosong = (TextView) findViewById(R.id.modul_kosong);
    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(Modul.this, null, "Please wait...", true, false);

        Call<List<ModelModul>> call = service.getModul(id_praktikum);
        call.enqueue(new Callback<List<ModelModul>>() {
            @Override
            public void onResponse(Call<List<ModelModul>> call, Response<List<ModelModul>> response) {
                data_modul.clear();

                if (response.isSuccessful()){
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        ModelModul data = new ModelModul(
                                response.body().get(i).getNamaModul(),
                                response.body().get(i).getIdModul()
                        );

                        data_modul.add(data);
                    }

                    adapter = new ListModul(Modul.this, R.layout.item_modul, data_modul);
                    listview.setAdapter(adapter);

                    if (adapter.getCount() < 1 ){
                        listview.setVisibility(View.INVISIBLE);
                        ModKosong.setVisibility(View.VISIBLE);
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<ModelModul>> call, Throwable t) {
                Toast.makeText(Modul.this, "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1){
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        data_modul.clear();
        adapter = new ListModul(Modul.this, R.layout.item_modul, data_modul);
        listview.setAdapter(adapter);
        setup();
    }
}
