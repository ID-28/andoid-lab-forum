package com.example.asisten.Modul;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.asisten.CustomOnItemClickListener;
import com.example.asisten.Materi.Materi;
import com.example.asisten.R;

import java.util.ArrayList;

public class ListModul extends ArrayAdapter<ModelModul> {
    private ArrayList<ModelModul> list;
    private LayoutInflater inflater;
    private int res;

    public ListModul(@NonNull Context context, int resource, @NonNull ArrayList<ModelModul> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ListModul.MyHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(res, parent, false);

            holder = new ListModul.MyHolder();

            holder.ModulName = (TextView) convertView.findViewById(R.id.nama_modul);
            holder.Modul = (CardView) convertView.findViewById(R.id.cardview_modul);

            convertView.setTag(holder);
        } else {
            holder = (ListModul.MyHolder) convertView.getTag();
        }

        holder.ModulName.setText(list.get(position).getNamaModul());
        holder.Modul.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, Materi.class);
                intent.putExtra("id_modul", String.valueOf(list.get(position).getIdModul()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(ModelModul object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder {
        TextView ModulName;
        CardView Modul;
    }
}
