package com.example.asisten;

import com.example.asisten.DetailThread.ModelDetailThread;
import com.example.asisten.Komen.ModelKomen;
import com.example.asisten.Materi.ModelMateri;
import com.example.asisten.Modul.ModelModul;
import com.example.asisten.Praktikum.ModelPraktikum;
import com.example.asisten.ThreadList.ModelListThread;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    @FormUrlEncoded
    @POST("login-aslab")
    Call<ModelUser> getLoginData(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("aslab/thread")
    Call<List<ModelPraktikum>> getPraktikum(@Field("username") String username);

    @GET("aslab/thread/{praktikum}")
    Call<List<ModelModul>> getModul(@Path("praktikum") String praktikum);

    @GET("aslab/thread/modul/{modul}")
    Call<List<ModelMateri>> getMateri(@Path("modul") String modul);

    @GET("aslab/thread/materi/{materi}")
    Call<List<ModelListThread>> getThreadList(@Path("materi") String materi);

    @GET("aslab/thread/detail/{thread}")
    Call<ModelDetailThread> getThreadDetail(@Path("thread") String thread);

    @GET("aslab/thread/comment/praktikan/{thread}")
    Call<List<ModelKomen>> getKomenPraktikan(@Path("thread") String thread);

    @GET("aslab/thread/comment/dosen/{thread}")
    Call<List<ModelKomen>> getKomenDosen(@Path("thread") String thread);

    @GET("aslab/thread/comment/aslab/{thread}")
    Call<List<ModelKomen>> getKomenAslab(@Path("thread") String thread);

    @FormUrlEncoded
    @POST("aslab/comment/create/{thread}")
    Call<ModelKomen> addComent (@Field("comment") String comment, @Field("user_id") String user_id, @Path("thread") String thread);


}
