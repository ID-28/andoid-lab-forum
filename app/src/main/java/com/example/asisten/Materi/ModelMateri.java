package com.example.asisten.Materi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelMateri {
    @SerializedName("namaMateri")
    @Expose
    private String namaMateri;

    @SerializedName("id_materi")
    @Expose
    private int id_materi;

    public ModelMateri(String namaMateri, int id_materi){
        this.namaMateri = namaMateri;
        this.id_materi = id_materi;
    }

    public String getNamaMateri() {
        return namaMateri;
    }

    public void setNamaMateri(String namaMateri) {
        this.namaMateri = namaMateri;
    }

    public int getId_materi() {
        return id_materi;
    }

    public void setId_materi(int id_materi) {
        this.id_materi = id_materi;
    }
}
