package com.example.asisten.Materi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.asisten.ApiService;
import com.example.asisten.MainActivity;
import com.example.asisten.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Materi extends AppCompatActivity {
    ArrayList<ModelMateri> data_materi = new ArrayList<ModelMateri>();
    ListView listview;
    ListMateri adapter;
    TextView matKos;

    ProgressDialog loading;

    String id_modul;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materi);

        id_modul = getIntent().getStringExtra("id_modul");

        listview = (ListView) findViewById(R.id.list_materi);
        listview.setDividerHeight(0);

        matKos = (TextView) findViewById(R.id.materi_kosong);
    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(Materi.this, null, "Please wait...", true, false);

        Call<List<ModelMateri>> call = service.getMateri(id_modul);
        call.enqueue(new Callback<List<ModelMateri>>() {
            @Override
            public void onResponse(Call<List<ModelMateri>> call, Response<List<ModelMateri>> response) {
                data_materi.clear();

                if (response.isSuccessful()){
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        ModelMateri data = new ModelMateri(
                                response.body().get(i).getNamaMateri(),
                                response.body().get(i).getId_materi()
                        );
                        data_materi.add(data);
                    }

                    adapter = new ListMateri(Materi.this, R.layout.item_materi, data_materi);
                    listview.setAdapter(adapter);

                    if (adapter.getCount() < 1 ){
                        listview.setVisibility(View.INVISIBLE);
                        matKos.setVisibility(View.VISIBLE);
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<ModelMateri>> call, Throwable t) {
                Toast.makeText(Materi.this, "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 ){
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        data_materi.clear();
        adapter = new ListMateri(Materi.this, R.layout.item_materi, data_materi);
        listview.setAdapter(adapter);
        setup();
    }
}
