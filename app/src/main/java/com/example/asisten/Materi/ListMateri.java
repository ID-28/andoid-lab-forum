package com.example.asisten.Materi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.asisten.CustomOnItemClickListener;
import com.example.asisten.R;
import com.example.asisten.ThreadList.ThreadList;

import java.util.ArrayList;

public class ListMateri extends ArrayAdapter <ModelMateri> {

    private ArrayList<ModelMateri> list;
    private LayoutInflater inflater;
    private int res;

    public ListMateri(@NonNull Context context, int resource, @NonNull ArrayList<ModelMateri> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ListMateri.MyHolder holder = null;

        if (convertView == null){
            convertView = inflater.inflate(res, parent, false);

            holder = new ListMateri.MyHolder();

            holder.SubjectName = (TextView) convertView.findViewById(R.id.nama_materi);
            holder.Materi = (CardView) convertView.findViewById(R.id.cardview_materi);

            convertView.setTag(holder);
        } else {
            holder = (ListMateri.MyHolder) convertView.getTag();
        }

        holder.SubjectName.setText(list.get(position).getNamaMateri() + " " + list.get(position).getId_materi());
        holder.Materi.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, ThreadList.class);
                intent.putExtra("id_materi", String.valueOf(list.get(position).getId_materi()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));
        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(ModelMateri object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder {
        TextView SubjectName;
        CardView Materi;
    }
}
