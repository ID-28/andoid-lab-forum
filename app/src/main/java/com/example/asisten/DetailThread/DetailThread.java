package com.example.asisten.DetailThread;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.asisten.ApiService;
import com.example.asisten.Komen.KomenAslab;
import com.example.asisten.Komen.KomenDosen;
import com.example.asisten.Komen.KomenPraktikan;
import com.example.asisten.MainActivity;
import com.example.asisten.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailThread extends AppCompatActivity {
    ProgressDialog loading;

    String idThread;

    TextView ThreadJudul, Nama, Ket;
    Button btn_komendosen, btn_komenaslab, btn_komenpraktikan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_thread);

        idThread = getIntent().getStringExtra("idThread");

        btn_komenaslab = (Button) findViewById(R.id.komenaslab);
        btn_komenaslab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailThread.this, KomenAslab.class);
                intent.putExtra("idThread", idThread);
                startActivity(intent);
            }
        });

        btn_komendosen = (Button) findViewById(R.id.komendosen);
        btn_komendosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailThread.this, KomenDosen.class);
                intent.putExtra("idThread", idThread);
                startActivity(intent);
            }
        });

        btn_komenpraktikan = (Button) findViewById(R.id.komenpraktikan);
        btn_komenpraktikan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailThread.this, KomenPraktikan.class);
                intent.putExtra("idThread", idThread);
                startActivity(intent);
            }
        });

        ThreadJudul = (TextView) findViewById(R.id.detail_judul);
        Nama = (TextView) findViewById(R.id.detail_nama);
        Ket = (TextView) findViewById(R.id.detail_keterangan);

        detailThread();
    }

    public void detailThread(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(DetailThread.this, null, "Please wait...", true, false);

        Call<ModelDetailThread> call = service.getThreadDetail(idThread);
        call.enqueue(new Callback<ModelDetailThread>() {
            @Override
            public void onResponse(Call<ModelDetailThread> call, Response<ModelDetailThread> response) {
                ModelDetailThread moDetail = response.body();
                assert moDetail != null;

                ThreadJudul.setText(moDetail.getJudulThread());
                Nama.setText("Name : " + moDetail.getNamaMahasiswa() + " || NPM : " + moDetail.getUsername() + " at " + moDetail.getPosted() +"\nasked : ");
                Ket.setText(moDetail.getKet());

                loading.dismiss();
            }

            @Override
            public void onFailure(Call<ModelDetailThread> call, Throwable t) {
                Toast.makeText(DetailThread.this, "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }
}
