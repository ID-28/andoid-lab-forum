package com.example.asisten.DetailThread;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.asisten.CustomOnItemClickListener;
import com.example.asisten.Komen.KomenAslab;
import com.example.asisten.Komen.KomenDosen;
import com.example.asisten.Komen.KomenPraktikan;
import com.example.asisten.R;

import java.util.ArrayList;

public class ListDetailThread extends ArrayAdapter <ModelDetailThread> {
    private ArrayList<ModelDetailThread> list;
    private LayoutInflater inflater;
    private int res;

    public ListDetailThread(@NonNull Context context, int resource, ArrayList<ModelDetailThread> list){
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        ListDetailThread.MyHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(res, parent, false);

            holder = new ListDetailThread.MyHolder();

            holder.JudulThread = (TextView) convertView.findViewById(R.id.detail_judul);
            holder.Nama = (TextView) convertView.findViewById(R.id.detail_nama);
            holder.Ket = (TextView) convertView.findViewById(R.id.detail_keterangan);

            holder.KomenAslab = (Button) convertView.findViewById(R.id.komenaslab);
            holder.KomenDosen = (Button) convertView.findViewById(R.id.komendosen);
            holder.Komenpraktikan = (Button) convertView.findViewById(R.id.komenpraktikan);

            convertView.setTag(holder);
        } else {
            holder = (ListDetailThread.MyHolder) convertView.getTag();
        }

        holder.JudulThread.setText(list.get(position).getJudulThread());
        holder.Nama.setText(list.get(position).getNamaMahasiswa());
        holder.Ket.setText(list.get(position).getKet());

        holder.KomenAslab.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, KomenAslab.class);
                intent.putExtra("nama_materi", String.valueOf(list.get(position).getIdThread()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));

        holder.KomenDosen.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, KomenDosen.class);
                intent.putExtra("nama_materi", String.valueOf(list.get(position).getIdThread()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));

        holder.Komenpraktikan.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, KomenPraktikan.class);
                intent.putExtra("nama_materi", String.valueOf(list.get(position).getIdThread()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(ModelDetailThread object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder{
        TextView JudulThread;
        TextView Nama;
        TextView Username;
        TextView Ket;

        Button KomenDosen;
        Button KomenAslab;
        Button Komenpraktikan;

    }
}
