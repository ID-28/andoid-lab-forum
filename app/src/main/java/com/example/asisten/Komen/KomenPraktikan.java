package com.example.asisten.Komen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.asisten.ApiService;
import com.example.asisten.MainActivity;
import com.example.asisten.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class KomenPraktikan extends AppCompatActivity {
    ArrayList<ModelKomen> data_komen_praktikan = new ArrayList<ModelKomen>();
    ListView listview;
    ListKomen adapter;

    ProgressDialog loading;

    TextView komen_praktikan_kosong;
    String idThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.komen_praktikan);

        idThread = getIntent().getStringExtra("idThread");

        listview = (ListView) findViewById(R.id.list_komentar_praktikan);
        listview.setDividerHeight(0);

        komen_praktikan_kosong = (TextView) findViewById(R.id.komentar_praktikan_kosong);

    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(KomenPraktikan.this, null, "Please wait", true, false);

        Call<List<ModelKomen>> call = service.getKomenPraktikan(idThread);
        call.enqueue(new Callback<List<ModelKomen>>() {
            @Override
            public void onResponse(Call<List<ModelKomen>> call, Response<List<ModelKomen>> response) {
                data_komen_praktikan.clear();

                if (response.isSuccessful()){
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        ModelKomen data = new ModelKomen(
                                response.body().get(i).getNama(),
                                response.body().get(i).getComment(),
                                response.body().get(i).getCreated_at(),
                                response.body().get(i).getUsername(),
                                response.body().get(i).getStatus(),
                                response.body().get(i).getIdComment(),
                                response.body().get(i).getIdThread()
                        );
                        data_komen_praktikan.add(data);
                    }

                    adapter = new ListKomen(KomenPraktikan.this, R.layout.item_komen, data_komen_praktikan);

                    listview.setAdapter(adapter);

                    if (adapter.getCount() < 1){
                        listview.setVisibility(View.INVISIBLE);
                        komen_praktikan_kosong.setVisibility(View.VISIBLE);
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<ModelKomen>> call, Throwable t) {
                Toast.makeText(KomenPraktikan.this, "Error Retrive Data from Server!!!\n" + t.getMessage(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        data_komen_praktikan.clear();
        adapter = new ListKomen(KomenPraktikan.this, R.layout.item_komen, data_komen_praktikan);
        listview.setAdapter(adapter);
        setup();
    }
}
