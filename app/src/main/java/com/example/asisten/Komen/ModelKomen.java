package com.example.asisten.Komen;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelKomen {
    @SerializedName("idComment")
    @Expose
    private int idComment;

    @SerializedName("idThread")
    @Expose
    private int idThread;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("status")
    @Expose
    private int status;

    public ModelKomen (String nama, String comment, String created_at, String username, int status, int idComment, int idThread){
        this.nama = nama;
        this.comment = comment;
        this.created_at = created_at;
        this.username = username;
        this.status = status;
        this.idComment = idComment;
        this.idThread = idThread;
    }

    public int getIdComment() {
        return idComment;
    }

    public void setIdComment(int idComment) {
        this.idComment = idComment;
    }

    public int getIdThread() {
        return idThread;
    }

    public void setIdThread(int idThread) {
        this.idThread = idThread;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
