package com.example.asisten.ThreadList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.asisten.ApiService;
import com.example.asisten.MainActivity;
import com.example.asisten.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ThreadList extends AppCompatActivity {
    ArrayList<ModelListThread> data_thread = new ArrayList<ModelListThread>();
    ListView listview;
    ListThread adapter;

    TextView listKosong;

    ProgressDialog loading;

    String id_materi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_thread);

        listview = (ListView) findViewById(R.id.list_thread);
        listview.setDividerHeight(0);

        id_materi = getIntent().getStringExtra("id_materi");

        listKosong = (TextView) findViewById(R.id.thread_kosong);

    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(ThreadList.this, null, "Please wait...", true,  false);

        Call<List<ModelListThread>> call = service.getThreadList(id_materi);
        call.enqueue(new Callback<List<ModelListThread>>() {
            @Override
            public void onResponse(Call<List<ModelListThread>> call, Response<List<ModelListThread>> response) {
                data_thread.clear();

                if (response.isSuccessful()){
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        ModelListThread data = new ModelListThread(
                                response.body().get(i).getIdThread(),
                                response.body().get(i).getJudul(),
                                response.body().get(i).getKeterangan(),
                                response.body().get(i).getNamaMahasiswa()
                        );

                        data_thread.add(data);
                    }

                    adapter = new ListThread(ThreadList.this, R.layout.item_list_thread, data_thread);
                    listview.setAdapter(adapter);

                    if (adapter.getCount() < 1 ){
                        listview.setVisibility(View.INVISIBLE);
                        listKosong.setVisibility(View.VISIBLE);
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<ModelListThread>> call, Throwable t) {
                Toast.makeText(ThreadList.this, "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        data_thread.clear();
        adapter = new ListThread(ThreadList.this, R.layout.item_list_thread, data_thread);
        listview.setAdapter(adapter);
        setup();
    }
}
