package com.example.asisten.ThreadList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.asisten.CustomOnItemClickListener;
import com.example.asisten.DetailThread.DetailThread;
import com.example.asisten.R;
import com.google.gson.internal.$Gson$Preconditions;

import java.util.ArrayList;

public class ListThread extends ArrayAdapter <ModelListThread> {
    private ArrayList<ModelListThread> list;
    private LayoutInflater inflater;
    private int res;

    public ListThread(@NonNull Context context, int resource, @NonNull ArrayList<ModelListThread> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListThread.MyHolder holder = null;

        if (convertView == null){
            convertView = inflater.inflate(res, parent, false);

            holder = new ListThread.MyHolder();

            holder.Judul = (TextView) convertView.findViewById(R.id.judul_thread);
            holder.Keterangan = (TextView) convertView.findViewById(R.id.keterangan_thread);
            holder.Nama = (TextView) convertView.findViewById(R.id.nama_mahasiswa_thread);
            holder.ThreadList = (CardView) convertView.findViewById(R.id.cardview_thread);

            convertView.setTag(holder);
        } else {
            holder = (ListThread.MyHolder) convertView.getTag();
        }

        holder.Judul.setText(list.get(position).getJudul());
        holder.Keterangan.setText(list.get(position).getKeterangan());
        holder.Nama.setText(list.get(position).getNamaMahasiswa());
        holder.ThreadList.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, DetailThread.class);
                intent.putExtra("idThread", String.valueOf(list.get(position).getIdThread()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(ModelListThread object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder {
        TextView Judul;
        TextView Keterangan;
        TextView Nama;

        CardView ThreadList;
    }
}
