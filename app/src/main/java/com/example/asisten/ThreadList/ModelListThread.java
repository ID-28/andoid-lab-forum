package com.example.asisten.ThreadList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelListThread {
    @SerializedName("idMateri")
    @Expose
    private int idMateri;

    @SerializedName("materi")
    @Expose
    private String materi;

    @SerializedName("idThread")
    @Expose
    private int idThread;

    @SerializedName("judul")
    @Expose
    private String judul;

    @SerializedName("keterangan")
    @Expose
    private String keterangan;

    @SerializedName("namaMahasiswa")
    @Expose
    private String namaMahasiswa;

    public ModelListThread(int idThread, String judul, String keterangan, String namaMahasiswa){
        this.idThread = idThread;
        this.judul = judul;
        this.keterangan = keterangan;
        this.namaMahasiswa = namaMahasiswa;
    }

    public int getIdMateri() {
        return idMateri;
    }

    public void setIdMateri(int idMateri) {
        this.idMateri = idMateri;
    }

    public String getMateri() {
        return materi;
    }

    public void setMateri(String materi) {
        this.materi = materi;
    }

    public int getIdThread() {
        return idThread;
    }

    public void setIdThread(int idThread) {
        this.idThread = idThread;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNamaMahasiswa() {
        return namaMahasiswa;
    }

    public void setNamaMahasiswa(String namaMahasiswa) {
        this.namaMahasiswa = namaMahasiswa;
    }
}
